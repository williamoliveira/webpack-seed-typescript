const path = require('path');
/* eslint-disable @typescript-eslint/no-var-requires */
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'source-map',
  devServer: {
    port: 8000,
    static: {
      directory: path.resolve(__dirname, './src'),
    },
    historyApiFallback: {
      index: 'index.html',
    },
  },
});
